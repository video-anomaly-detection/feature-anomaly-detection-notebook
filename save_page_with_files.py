import argparse
import os, sys
import shutil
from urllib.parse import urljoin, urlparse
from bs4 import BeautifulSoup
import re

def savePage(url, pagefilename='page'):
    def soupfindnSave(pagefolder, tag2find='img', inner='src'):
        """saves on specified `pagefolder` all tag2find objects"""
        if not os.path.exists(pagefolder): # create only once
            os.mkdir(pagefolder)
        for res in soup.findAll(tag2find):   # images, css, etc..
                if not res.has_attr(inner): # check if inner tag (file object) exists
                    continue # may or may not exist
                # filename = re.sub('\W+', '', os.path.basename(res[inner])) # clean special chars
                filename = res[inner]
                if filename.startswith('data:image/png;base64,'):
                    # If it's encoded in base64, not a URL, we don't need to do anything, we can just skip it.
                    continue
                if len(filename) > 128:
                    raise ValueError(res)
                fileurl = urljoin(url, res.get(inner))
                if fileurl.startswith('http'):
                    print('not downloading', fileurl)
                    continue
                #filepath = os.path.join(pagefolder, filename)
                filepath = os.path.join(os.path.dirname(pagefilename), filename)
                # rename html ref so can move html and folder of files anywhere
                # res[inner] = os.path.join(os.path.basename(pagefolder), filename)
                if not os.path.isfile(filepath): # was not downloaded
                    os.makedirs(os.path.dirname(filepath), exist_ok=True)
                    with open(filepath, 'wb') as file:
                        file.write(open(fileurl, 'rb').read())
                    print('saved', filepath)
        return soup
    
    soup = BeautifulSoup(open(url).read(), features="lxml")
    pagefolder = pagefilename+'_files' # page contents
    soup = soupfindnSave(pagefolder, 'img', 'src')
    soup = soupfindnSave(pagefolder, 'link', 'href')
    soup = soupfindnSave(pagefolder, 'script', 'src')
    soup = soupfindnSave(pagefolder, 'source', 'src')
    shutil.copy(url, pagefilename+'.html')
    return soup

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Save HTML page with files.')
    parser.add_argument('filepath', nargs='?', default='notebook.html', help='HTML file to save.')
    args = parser.parse_args()
    savePage(args.filepath, pagefilename=os.path.join('public', os.path.splitext(args.filepath)[0]))
